FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/rest-service-thorntail.jar /usr/src
CMD java -Xmx320m -ea -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses=true -jar rest-service-thorntail.jar
