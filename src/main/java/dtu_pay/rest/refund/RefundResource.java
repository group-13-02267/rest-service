package dtu_pay.rest.refund;

import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.RestResponses;
import dtu_pay.rest.payment.PaymentRequestDTO;
import facades.DTUMerchantFacade;
import facades.MerchantFacade;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * Resource involving refunding
 * @author Roberts
 */
@Path("/refund")
public class RefundResource {

    /**
     * The merchant facade
     */
    MerchantFacade merchantFacade;

    /**
     * Instantiate a new Refund Resource with the default facade
     */
    public RefundResource() {
        merchantFacade = DTUMerchantFacade.create();
    }

    /**
     * Instantiate a new Refund Resource with a custom facade
     * @param merchantFacade The custom facade
     */
    public RefundResource(MerchantFacade merchantFacade) {
        this.merchantFacade = merchantFacade;
    }

    /**
     * Initiate a new refund
     * @param request The refund request DTO
     * @return A response indicating the refund status
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response initiateRefund(PaymentRequestDTO request) {

        if (request == null) {
            return RestResponses.createErrorResponse(BAD_REQUEST, "Invalid JSON");
        }

        DTUPayEmptyResponse response = merchantFacade.refundPayment(request.token, request.merchantId, request.amount);
        if (response == null) {
            return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, "the payment could not be refunded");
        }
        if (!response.success()) {
            return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, response.getErrorMessage().getMessage());
        }
        return RestResponses.createSuccessResponse();
    }
}
