package dtu_pay.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * The main rest application class
 * @author Mathias, Nick, Robert, David and George
 */
@ApplicationPath("/")
public class RestApplication extends Application {

}
