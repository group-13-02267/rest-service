package dtu_pay.rest.payment;

/**
 * DTO for requesting a payment
 * @author Georg
 */
public class PaymentRequestDTO {
    public String token;
    public String merchantId;
    public float amount;
}
