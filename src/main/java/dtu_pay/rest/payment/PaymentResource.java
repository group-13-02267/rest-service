package dtu_pay.rest.payment;

import dtu_pay.rest.core.RestResponses;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import facades.DTUMerchantFacade;
import facades.MerchantFacade;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;


@Path("/payment")
/**
 * Resource involving payments
 * @author Georg
 */
public class PaymentResource {

    /**
     * The merchant facade
     */
    MerchantFacade merchantFacade;

    /**
     * Instantiate a new Payment Resource with the default Merchant Facade
     */
    public PaymentResource() {
        merchantFacade = DTUMerchantFacade.create();
    }

    /**
     * Instantiate a new Payment Resource with custom facade
     * @param merchantFacade The custom facade
     */
    public PaymentResource(MerchantFacade merchantFacade) {
        this.merchantFacade = merchantFacade;
    }

    /**
     * Create a new payment
     * @param paymentDTO The payment request DTO
     * @return A response indicating if the payment creation was successful or not
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createPayment(PaymentRequestDTO paymentDTO) {

        if (paymentDTO == null) {
            return RestResponses.createErrorResponse(BAD_REQUEST, "Invalid JSON");
        }

        DTUPayEmptyResponse response = merchantFacade.createPayment(
                paymentDTO.token,
                paymentDTO.merchantId,
                paymentDTO.amount
        );

        System.out.println("response received: " + response);

        if (!response.success()){
            return RestResponses.createErrorResponse(BAD_REQUEST, response.getErrorMessage().getMessage());
        }
        return RestResponses.createSuccessResponse();
    }
}
