package dtu_pay.rest.customer;

import dtu_pay.common.DatetimeConverter;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.rest.core.RestResponses;
import dtu_pay.services.administration.DTO.CustomerDTO;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.token.DTO.TokenRequestDTO;
import dtu_pay.services.token.DTO.TokenResponseDTO;
import facades.CustomerFacade;
import facades.DTUCustomerFacade;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * Resource related to customer operations
 @author Mathias, David
 */
@Path("/customer")
public class CustomerResource {

    /**
     * The customer facade
     */
    private CustomerFacade customerFacade;

    /**
     * Instantiate a new CustomerResource
     * @param customerFacade The customer facade
     */
    public CustomerResource(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    /**
     * Instantiate a new CustomerResource with default CustomerFacade implementation
     */
    public CustomerResource() {
        customerFacade = DTUCustomerFacade.create();
    }

    /**
     * Create a new customer
     * @param customerDTO The new customer data
     * @return Successful response if provided the valid data,
     * fails if not all fields are provided or the customer creation could not be created
     */
    @POST
    @Path("/")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createCustomer(CustomerDTO customerDTO) {
        if (customerDTO == null) {
            return RestResponses.createInvalidJsonResponse();
        }

        DTUPayEmptyResponse response = customerFacade.createCustomer(customerDTO.firstName, customerDTO.lastName, customerDTO.cpr);

        if (response == null) {
        	return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, response.getErrorMessage().getMessage());
        } else if (!response.success()) {
        	return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, response.getErrorMessage().getMessage());
        }

        return RestResponses.createSuccessResponse();
    }

    /**
     * Get a report for the given customer
     * @param merchantId The customer id
     * @param from the from date
     * @return A response containing the report for the specified parameters
     */
    @GET
    @Path("/{customerId}/report/{from}/")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getReport(@PathParam("customerId") String merchantId, @PathParam("from") String from) {
        return getReport(merchantId, from, DatetimeConverter.convertDateToString(LocalDate.now().plusDays(1)));
    }

    /**
     * Get a report for the given customer
     * @param merchantId The customer id
     * @param from The from date
     * @param to The to date
     * @return A response containing the report for the specified parameters
     */
    @GET
    @Path("/{customerId}/report/{from}/{to}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getReport(@PathParam("customerId") String merchantId, @PathParam("from") String from,
                              @PathParam("to") String to) {

        if (from == null || to == null) {
            return RestResponses.createInvalidJsonResponse();
        }

        DTUPayResponse<ReportDTO> report = customerFacade.generateReport(merchantId, from, to);

        if (report == null) {
            return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, "something went wrong");
        }

        return RestResponses.createSuccessResponse(report);
    }

    /**
     * Generate a new set of tokens for a customer
     * @param customerId The customer id
     * @param request The token DTO with the amount of tokens
     * @return The generated tokens or fail if the customer has too many tokens fx.
     */
    @POST
    @Path("/{customerId}/token")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response requestTokens(@PathParam("customerId") String customerId, TokenRequestDTO request) {
        DTUPayResponse<TokenResponseDTO> tokensResponse = customerFacade.generateTokens(customerId, request.amount);

        if (tokensResponse == null || !tokensResponse.success()) {
            return RestResponses.createErrorResponse(BAD_REQUEST, "Could not generate the requested amount of tokens");
        }

        return RestResponses.createSuccessResponse(tokensResponse);
    }

}
