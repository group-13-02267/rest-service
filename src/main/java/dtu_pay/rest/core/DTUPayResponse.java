package dtu_pay.rest.core;

/**
 * Response interface which holds data
 *
 * @param <T> The data type
 */
public interface DTUPayResponse<T> extends DTUPayEmptyResponse {

    /**
     * Get the response
     *
     * @return The response
     */
    T getResponse();
}
