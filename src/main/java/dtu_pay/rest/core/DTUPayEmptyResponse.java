package dtu_pay.rest.core;

/**
 * Interface for an empty response
 *
 * @author Mathias
 */
public interface DTUPayEmptyResponse {

    /**
     * Get whether the response is successful
     *
     * @return True if successful, false if not
     */
    boolean success();

    /**
     * Get the error message
     *
     * @return The error message if present
     */
    ErrorMessage getErrorMessage();
}
