package dtu_pay.rest.core;

/**
 * Response that holds data
 *
 * @param <T> The data type
 * @author Mathias
 */
public class DTUPayResponseImpl<T> extends DTUPayEmptyResponseImpl implements DTUPayResponse<T> {

    /**
     * The response data
     */
    private T response;

    /**
     * Instantiate a successful response
     *
     * @param response The response data
     */
    public DTUPayResponseImpl(T response) {
        this.response = response;
    }

    /**
     * Instantiate an unsuccessful response
     *
     * @param errorMessage The error message
     */
    public DTUPayResponseImpl(ErrorMessage errorMessage) {
        super(errorMessage);
    }

    @Override
    public T getResponse() {
        return response;
    }
}
