package dtu_pay.rest.core;

import com.google.gson.Gson;

import javax.ws.rs.core.Response;
import java.io.Serializable;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

/**
 * Helper class for creating REST responses
 * @author Mathias
 */
public class RestResponses {

    /**
     * Create a successful response
     * @param responseObj The response data
     * @param <T> The response data type
     * @return The Successful response with status 200 and the response data
     */
    public static <T> Response createSuccessResponse(T responseObj) {
        return Response
                .status(Response.Status.OK)
                .entity(serializeObject(responseObj))
                .build();
    }

    /**
     * Create a successful but empty response
     * @return The successful empty response with status 2000
     */
    public static Response createSuccessResponse() {
        return Response
                .noContent()
                .status(Response.Status.OK)
                .build();
    }

    /**
     * Create an error response
     * @param errorClassifier The error status
     * @param errorMessage The error message
     * @return The error response with the provided status and error message
     */
    public static Response createErrorResponse(Response.Status errorClassifier, String errorMessage) {
        return Response
                .status(errorClassifier)
                .entity(serializeObject(new ErrorMessageDTO(errorMessage)))
                .build();
    }

    /**
     * Create an error response targeting invalid json
     * @return The error response with status 400 and invalid json error message
     */
    public static Response createInvalidJsonResponse() {
        return Response.status(BAD_REQUEST).entity(serializeObject(new ErrorMessageDTO("Invalid JSON"))).build();
    }

    /**
     * Serialize an object to JSON
     * @param object The object to serialize
     * @param <T> The object type
     * @return The JSON representation as a String
     */
    private static <T> String serializeObject(T object) {
        return new Gson().toJson(object);
    }
}

/**
 * Error message DTO
 * @author Mathias
 */
class ErrorMessageDTO implements Serializable {

    /**
     * The error message
     */
    public final String errorMessage;

    /**
     * Instantiate a new error message DTO
     * @param error The error message
     */
    public ErrorMessageDTO(String error) {
        this.errorMessage = error;
    }
}
