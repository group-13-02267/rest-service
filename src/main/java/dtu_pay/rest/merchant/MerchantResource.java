package dtu_pay.rest.merchant;

import dtu_pay.common.DatetimeConverter;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.rest.core.RestResponses;
import dtu_pay.services.reporting.DTO.ReportDTO;
import facades.DTUMerchantFacade;
import facades.MerchantFacade;
import dtu_pay.services.administration.DTO.MerchantDTO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



import java.time.LocalDate;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * Resource for all the merchant endpoints
 * @author Mathias, Roberts, Nick
 */
@Path("/merchant")
public class MerchantResource {

    /**
     * The merchant facade
     */
    private MerchantFacade merchantFacade;

    /**
     * Instantiate a new MerchantResource
     * @param merchantFacade The merchant facade that the endpoints will use
     */
    public MerchantResource(MerchantFacade merchantFacade) {
        this.merchantFacade = merchantFacade;
    }

    /**
     * Instantiate a new MerchantResource with the default MerchantFacade
     */
    public MerchantResource() {
        merchantFacade = DTUMerchantFacade.create();
    }

    /**
     * Create a new Merchant
     * @param merchantDTO The merchant DTO
     * @return A response with status 200 if the merchant is created, 400 if the creation fails
     */
    @POST
    @Path("/")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createMerchant(MerchantDTO merchantDTO) {

        if (merchantDTO == null) {
            return RestResponses.createInvalidJsonResponse();
        }

        DTUPayEmptyResponse response = merchantFacade.createMerchant(merchantDTO.firstName, merchantDTO.lastName, merchantDTO.cpr);
        
        if (response == null) {
        	return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, response.getErrorMessage().getMessage());
        } else if (!response.success()) {
        	return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, response.getErrorMessage().getMessage());
        }

        return RestResponses.createSuccessResponse();
    }

    /**
     * Get a merchant report from to now
     * @param merchantId The merchant id
     * @param from The from date
     * @return The report for the merchant within the given parameters
     */
    @GET
    @Path("/{merchantId}/report/{from}/")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getReport(@PathParam("merchantId") String merchantId, @PathParam("from") String from) {
        return getReport(merchantId, from, DatetimeConverter.convertDateToString(LocalDate.now().plusDays(1)));
    }

    /**
     * Get a merchant report from to now
     * @param merchantId The merchant id
     * @param from The from date
     * @param to The to date
     * @return The report for the merchant within the given parameters
     */
    @GET
    @Path("/{merchantId}/report/{from}/{to}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getReport(@PathParam("merchantId") String merchantId, @PathParam("from") String from,
                              @PathParam("to") String to) {
        if (from == null || to == null) {
            return RestResponses.createInvalidJsonResponse();
        }

        DTUPayResponse<ReportDTO> report = merchantFacade.generateReport(merchantId, from, to);

        if (report == null) {
            return RestResponses.createErrorResponse(INTERNAL_SERVER_ERROR, "something went wrong");
        }

        return RestResponses.createSuccessResponse(report);
    }
}
