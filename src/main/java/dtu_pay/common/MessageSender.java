package dtu_pay.common;

/**
 * Interface for implementing a Message Sender
 */
public interface MessageSender {
    /**
     * Send a new message
     * @param content The message content
     * @param route The message route
     * @param exchange The exchange to send the message on
     */
    void sendMessage(Object content, String route, String exchange);
}
