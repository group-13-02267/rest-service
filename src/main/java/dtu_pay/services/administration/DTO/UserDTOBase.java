package dtu_pay.services.administration.DTO;

/**
 * Base DTO for DTUPay users
 */
public class UserDTOBase {
    /**
     * The User id
     */
	public String instanceId;

    /**
     * The user cpr
     */
    public String cpr;

    /**
     * The user first name
     */
    public String firstName;

    /**
     * The user last name
     */
    public String lastName;
}
