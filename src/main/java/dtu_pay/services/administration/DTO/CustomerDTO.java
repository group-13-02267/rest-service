package dtu_pay.services.administration.DTO;

/**
 * Customer DTO
 */
public class CustomerDTO extends UserDTOBase {

    /**
     * Instantiate a new CustomerDTO
     */
    public CustomerDTO() {
    }

    public CustomerDTO(String first, String last, String cpr) {
        this.firstName = first;
        this.lastName = last;
        this.cpr = cpr;
    }
}
