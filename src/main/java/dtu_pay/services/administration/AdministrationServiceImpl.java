package dtu_pay.services.administration;

import com.google.gson.Gson;
import core.AsyncService;
import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayEmptyResponseImpl;
import dtu_pay.services.ExchangeProvider;
import dtu_pay.services.administration.DTO.CustomerDTO;
import dtu_pay.services.administration.DTO.MerchantDTO;
import dtu_pay.services.helpers.ResponseMessageHelper;
import dtu_pay.services.payment.DTO.ResponseDTO;

import java.util.concurrent.CompletableFuture;

/**
 * Administration service implementation of {@link AdministrationService}
 * Is an async service that listens on the creation of users it is responsible for.
 *
 * @author Roberts
 */
public class AdministrationServiceImpl extends AsyncService<ResponseDTO> implements AdministrationService {

	/**
	 * Future used to receive the response
	 */
	private CompletableFuture<ResponseDTO> future;

	/**
	 * Message sender for broadcasting messages
	 */
	private MessageSender administrationMessageSender;

	/**
	 * unique instance id
	 */
	private String instanceId;

	/**
	 * User type
	 */
	private String typeOfUser;

	/**
	 * Instantiate a new administration service
	 *
	 * @param typeOfUser                  The type of user responsible for
	 * @param instanceId                  The unique instance id
	 * @param administrationMessageSender The message sender
	 */
	public AdministrationServiceImpl(String typeOfUser, String instanceId, MessageSender administrationMessageSender) {
		super("administration", "administration." + typeOfUser + ".created." + instanceId);
		this.administrationMessageSender = administrationMessageSender;
		this.typeOfUser = typeOfUser;
		this.instanceId = instanceId;
		this.future = new CompletableFuture<>();
	}

	/**
	 * Instantiate a new administration service for a specific future
	 *
	 * @param typeOfUser                  The type of user responsible for
	 * @param instanceId                  The unique instance id
	 * @param future                      A completable future to be used
	 * @param administrationMessageSender The message sender
	 */
	public AdministrationServiceImpl(String typeOfUser, String instanceId, MessageSender administrationMessageSender, CompletableFuture<ResponseDTO> future) {
		super("administration", "administration." + typeOfUser + ".created." + instanceId);
		this.administrationMessageSender = administrationMessageSender;
		this.typeOfUser = typeOfUser;
		this.instanceId = instanceId;
		this.future = future;
	}

	public DTUPayEmptyResponse createMerchant(String firstName, String lastName, String cpr) {
		MerchantDTO merchantDTO = new MerchantDTO();
		merchantDTO.firstName = firstName;
		merchantDTO.lastName = lastName;
		merchantDTO.cpr = cpr;
		merchantDTO.instanceId = instanceId;

		boolean isSend = trySendMerchant(merchantDTO);

		ResponseDTO response = future.join();

		if (response.success && isSend) {
			return new DTUPayEmptyResponseImpl();
		}
		return ResponseMessageHelper.CreateEmptyErrorResponse("Something went wrong when creating a merchant");
	}

	public DTUPayEmptyResponse createCustomer(String firstName, String lastName, String cpr) {
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.firstName = firstName;
		customerDTO.lastName = lastName;
		customerDTO.cpr = cpr;
		customerDTO.instanceId = instanceId;

		boolean isSend = trySendCustomer(customerDTO);

		ResponseDTO response = future.join();

		if (response.success && isSend) {
			return new DTUPayEmptyResponseImpl();
		}
		return ResponseMessageHelper.CreateEmptyErrorResponse("Something went wrong when creating a customer");


	}

	@Override
	protected ResponseDTO handleMessage(String message) {
		return new Gson().fromJson(message, ResponseDTO.class);
	}

	@Override
	public void receive(ResponseDTO received) {
		future.complete(received);
	}

	/**
	 * Send message trying to create a new merchant
	 *
	 * @param merchant The merchant to be created
	 * @return Boolean representing the creation status
	 */
	private boolean trySendMerchant(MerchantDTO merchant) {
		try {
			administrationMessageSender.sendMessage(merchant, "administration." + typeOfUser + ".create." + instanceId, ExchangeProvider.getAdministrationExchange());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Send message trying to create a new customer
	 *
	 * @param customer The customer to be created
	 * @return Boolean representing the creation status
	 */
	private boolean trySendCustomer(CustomerDTO customer) {
		try {
			administrationMessageSender.sendMessage(customer, "administration." + typeOfUser + ".create." + instanceId, ExchangeProvider.getAdministrationExchange());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
