package dtu_pay.services.administration;

import dtu_pay.rest.core.DTUPayEmptyResponse;

/**
 * Interface for handling the administration service
 * @author Roberts
 */
public interface AdministrationService {

	/**
	 * Create a new merchant
	 * @param firstName Merchant first name
	 * @param lastName Merchant last name
	 * @param cpr Merchant cpr
	 * @return Empty response with the status of the creation
	 */
	DTUPayEmptyResponse createMerchant(String firstName, String lastName, String cpr);

	/**
	 * Create a new customer
	 * @param firstName Customer first name
	 * @param lastName Customer last name
	 * @param cpr Customer cpr
	 * @return Empty response with the status of the creation
	 */
	DTUPayEmptyResponse createCustomer(String firstName, String lastName, String cpr);
}
