package dtu_pay.services.administration;

import java.util.UUID;

import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.ServiceFactoryBase;

/**
 * Factory for creating administration service handlers
 *
 * @author Roberts
 */
public class AdministrationServiceFactory extends ServiceFactoryBase<AdministrationServiceImpl> {

    /**
     * Type of users that the administration is responsible for
     */
    String typeOfUser;

    /**
     * Instantiate a new Administration Service Factory for a specific type of users
     * @param typeOfUser The type of users responsible for
     */
    public AdministrationServiceFactory(String typeOfUser) {
        this.typeOfUser = typeOfUser;
    }

    /**
     * Create a new administration service
     * @return The new administration service
     */
    @Override
    protected AdministrationServiceImpl createService() {
        return new AdministrationServiceImpl(typeOfUser, UUID.randomUUID().toString(), new RabbitMqMessageService());
    }

}
