package dtu_pay.services.helpers;

import java.util.concurrent.CompletableFuture;

/**
 * Interface for wrapping futures
 */
public interface FutureWrapper {

     /**
      * Join and get the response of the future
      * @param completableFuture The future to join and get the value of
      * @param <T> The future object type
      * @return The value from the future
      */
     <T> T join(CompletableFuture<T> completableFuture);
}
