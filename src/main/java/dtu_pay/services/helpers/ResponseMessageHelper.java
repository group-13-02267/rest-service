package dtu_pay.services.helpers;

import dtu_pay.rest.core.*;

/**
 * Helper class for handling responses
 * @author Mathias Ibsen
 */
public class ResponseMessageHelper {

    /**
     * Create an empty response with an error message
     * @param errorMsg The error message
     * @return The response with an error message
     */
    public static DTUPayEmptyResponse CreateEmptyErrorResponse(String errorMsg){
        return new DTUPayEmptyResponseImpl(new DTUPayErrorMessage(errorMsg));
    }

    /**
     * Create a none empty response with an error message
     * @param errorMsg The error message
     * @return The none empty response with an error message
     */
    public static DTUPayResponse CreateErrorResponse(String errorMsg) {
        return new DTUPayResponseImpl(new DTUPayErrorMessage(errorMsg));
    }
}
