package dtu_pay.services.helpers;

import java.util.concurrent.CompletableFuture;

/**
 * Helper class for wrapping a {@link CompletableFuture} which enables moqing of futures in tests
 * @author Mathias
 */
public class DTUFutureWrapper implements FutureWrapper {

    /**
     * Join and get the response of the future
     * @param completableFuture The future to join and get the value of
     * @param <T> The future object type
     * @return The value from the future
     */
    @Override
    public <T> T join(CompletableFuture<T> completableFuture) {
        return completableFuture.join();
    }
}
