package dtu_pay.services.payment;

import dtu_pay.rest.core.DTUPayEmptyResponse;

/**
 * Interface for handling payments
 * @author Mathias Ibsens
 */
public interface PaymentService {
    /**
     * Create a new payment
     * @param token_value The raw token value used for the payment
     * @param merchant_cpr The merchant CPR
     * @param amount The payment amount
     * @return An empty response indicating the successfulness of the creation
     */
    DTUPayEmptyResponse createPayment(String token_value, String merchant_cpr, float amount);
}
