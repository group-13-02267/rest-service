package dtu_pay.services.payment.DTO;

/**
 * Payment DTO
 * @author David, Mathias
 */
public class PaymentDTO {

    /**
     * Instantiate a new payment DTO
     * @param id The unique payment id
     * @param token The token used for the payment
     * @param receiver The payment receiver
     * @param amount The payment amount
     */
    public PaymentDTO(String id, String token, String receiver, float amount) {
        this.id = id;
        this.token = token;
        this.receiver = receiver;
        this.amount = amount;
    }

    public PaymentDTO() {
    }

    /**
     * The unique payment id
     */
    public String id;

    /**
     * The token used for the payment
     */
    public String token;

    /**
     * The payment receiver
     */
    public String receiver;

    /**
     * The payment amount
     */
    public float amount;
}
