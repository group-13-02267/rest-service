package dtu_pay.services.payment.DTO;

/**
 * Response DTO used for fx. creating refund and payments
 */
public class ResponseDTO {

    /**
     * Instantiate a new Response DTO
     * @param success The response status
     */
    public ResponseDTO(boolean success) {
        this.success = success;
    }

    public ResponseDTO() {
    }

    /**
     * The response status
     */
    public boolean success;
}
