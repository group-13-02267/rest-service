package dtu_pay.services.payment;

import com.google.gson.Gson;
import core.AsyncService;
import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayEmptyResponseImpl;
import dtu_pay.services.ExchangeProvider;
import dtu_pay.services.helpers.ResponseMessageHelper;
import dtu_pay.services.payment.DTO.PaymentDTO;
import dtu_pay.services.payment.DTO.ResponseDTO;

import java.util.concurrent.CompletableFuture;

/**
 * Implementation of the {@link PaymentService} and is implemented as an {@link AsyncService}
 * in order to receive messages when creating new payments
 * @author Mathias Ibsen
 */
public class PaymentServiceImpl extends AsyncService<ResponseDTO> implements PaymentService {

    /**
     * The future to receive the response on
     */
    private CompletableFuture<ResponseDTO> future;

    /**
     * The unique payment id
     */
    String paymentId;

    /**
     * The message sender
     */
    private MessageSender paymentMessageSender;

    /**
     * Instantiate a new Payment Service handler
     * @param paymentId The unique payment id
     * @param paymentMessageSender The message sender
     */
    public PaymentServiceImpl(String paymentId, MessageSender paymentMessageSender){
        super("payment", "transaction.payment.response." + paymentId);
        this.paymentMessageSender = paymentMessageSender;
        this.paymentId = paymentId;
    }

    public DTUPayEmptyResponse createPayment(String token_value, String merchant_cpr, float amount) {
        PaymentDTO paymentDTO = new PaymentDTO(paymentId, token_value, merchant_cpr, amount);
        future = new CompletableFuture<>();

        boolean isSend = trySendPayment(paymentDTO);

        ResponseDTO response = future.join();

        if (response.success && isSend){
            return new DTUPayEmptyResponseImpl();
        }
        return ResponseMessageHelper.CreateEmptyErrorResponse("Something went wrong when transfering the money");
    }

    @Override
    protected ResponseDTO handleMessage(String message) {
        return new Gson().fromJson(message, ResponseDTO.class);
    }

    @Override
    public void receive(ResponseDTO received) {
        future.complete(received);
    }

    /**
     * Try to send the payment for creation
     * @param payment The payment to be created
     * @return Boolean indicating if the creation was successful or not
     */
    private boolean trySendPayment(PaymentDTO payment){
        try {
            paymentMessageSender.sendMessage(payment, "transaction.payment.received." + paymentId, ExchangeProvider.getPaymentExchange());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
