package dtu_pay.services.payment;

import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.ServiceFactoryBase;

import java.util.UUID;

/**
 * Service factory for creating Payment Service handlers
 * @author Mathias Ibsen
 */
public class PaymentServiceFactory extends ServiceFactoryBase<PaymentServiceImpl> {
    @Override
    protected PaymentServiceImpl createService() {
        return new PaymentServiceImpl(UUID.randomUUID().toString(), new RabbitMqMessageService());
    }
}
