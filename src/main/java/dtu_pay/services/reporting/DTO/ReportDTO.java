package dtu_pay.services.reporting.DTO;

/**
 * Report Response DTO
 */
public class ReportDTO {

    /**
     * Report generation status
     */
    boolean success;

    /**
     * The report
     */
    String report;
}
