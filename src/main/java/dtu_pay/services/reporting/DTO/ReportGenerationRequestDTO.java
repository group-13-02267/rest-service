package dtu_pay.services.reporting.DTO;

/**
 * DTO for generating a new report
 */
public class ReportGenerationRequestDTO {

    /**
     * The unique request id
     */
    private String reportGenerationRequestId;

    /**
     * The user id
     */
    private String userId;

    /**
     * The from date
     */
    private String startDate;

    /**
     * The to date
     */
    private String endDate;

    /**
     * Instantiate a new ReportGenerationRequestDTO
     * @param reportGenerationRequestId The unique request id
     * @param userId The user id
     * @param startDate The from date
     * @param endDate The to date
     */
    public ReportGenerationRequestDTO(String reportGenerationRequestId, String userId, String startDate, String endDate)
    {
        this.reportGenerationRequestId = reportGenerationRequestId;
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
