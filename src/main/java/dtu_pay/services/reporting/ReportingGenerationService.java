package dtu_pay.services.reporting;

import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.ServiceFactory;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.customer.CustomerReportGeneration;
import dtu_pay.services.reporting.merchant.MerchantReportGeneration;

/**
 * Service for generating reports
 * @author Mathias Ibsen
 */
public class ReportingGenerationService implements ReportingGeneration {

    /**
     * Factory for generating Merchant Report Generator handlers
     */
    private final ServiceFactory<MerchantReportGeneration> merchantReportServiceFactory;

    /**
     * Factory for generating Customer Report Generator handlers
     */
    private final ServiceFactory<CustomerReportGeneration> customerReportServiceFactory;

    /**
     * Instantiate a new Reporting Generation Service
     * @param merchantReportServiceFactory The merchant report service factory
     * @param customerReportServiceFactory The customer report service factory
     */
    public ReportingGenerationService(ServiceFactory merchantReportServiceFactory, ServiceFactory customerReportServiceFactory) {
        this.merchantReportServiceFactory = merchantReportServiceFactory;
        this.customerReportServiceFactory = customerReportServiceFactory;
    }

    @Override
    public DTUPayResponse<ReportDTO> generateMerchantReport(String merchantId, String from, String to) {
        return merchantReportServiceFactory.setupService().generateMerchantReport(merchantId, from, to);
    }

    @Override
    public DTUPayResponse<ReportDTO> generateCustomerReport(String customerId, String from, String to) {
        return customerReportServiceFactory.setupService().generateCustomerReport(customerId, from, to);
    }
}
