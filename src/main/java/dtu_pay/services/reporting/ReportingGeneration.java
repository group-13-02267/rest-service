package dtu_pay.services.reporting;

import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.reporting.DTO.ReportDTO;

import java.time.LocalDate;

/**
 * Interface for handling report generation
 * @author Mathias
 */
public interface ReportingGeneration {

    /**
     * Generate a new merchant report
     * @param merchantId The merchant id
     * @param from The from date
     * @param to The to date
     * @return The generated report
     */
    DTUPayResponse<ReportDTO> generateMerchantReport(String merchantId, String from, String to);

    /**
     * Generate a new customer report
     * @param customerId The customer id
     * @param from The from date
     * @param to The to date
     * @return The generated report
     */
    DTUPayResponse<ReportDTO> generateCustomerReport(String customerId, String from, String to);
}
