package dtu_pay.services.reporting;

import com.google.gson.Gson;
import core.AsyncService;
import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.rest.core.DTUPayResponseImpl;
import dtu_pay.services.ExchangeProvider;
import dtu_pay.services.helpers.FutureWrapper;
import dtu_pay.services.helpers.ResponseMessageHelper;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.DTO.ReportGenerationRequestDTO;

import java.util.concurrent.CompletableFuture;

/**
 * AsyncService base for all report generation services
 * @author Mathias
 */
public abstract class ReportingGenerationServiceBase extends AsyncService<ReportDTO> {

    /**
     * The route to send on
     */
    private String sendRoute;

    /**
     * The future to receive the response on
     */
    private CompletableFuture<ReportDTO> future;

    /**
     * The message sender
     */
    private final MessageSender customerReportMessageSender;

    /**
     * The unique request id
     */
    private String requestId;

    /**
     * Wrapper for the future
     */
    private FutureWrapper futureWrapper;

    /**
     * Instantiate a new Reporting Generation Service Base as an Async Service
     * @param requestId The unique request id
     * @param receiveRoute The route to receive on
     * @param sendRoute The route send on
     * @param customerReportMessageSender The message sender
     * @param futureWrapper The {@link CompletableFuture} wrapper
     */
    public ReportingGenerationServiceBase(String requestId, String receiveRoute, String sendRoute,
                                          MessageSender customerReportMessageSender, FutureWrapper futureWrapper) {
        super(ExchangeProvider.getReportingExchange(), receiveRoute);
        this.sendRoute = sendRoute;
        this.customerReportMessageSender = customerReportMessageSender;
        this.requestId = requestId;
        this.futureWrapper = futureWrapper;
    }

    /**
     * Generate a new report
     * @param userId The user id to generate the report for
     * @param to The to date
     * @param from The from date
     * @return The report
     */
    protected DTUPayResponse<ReportDTO> generateReport(String userId, String to, String from) {
        ReportGenerationRequestDTO paymentDTO = new ReportGenerationRequestDTO(requestId, userId, to, from);
        future = new CompletableFuture<>();

        boolean isSend = trySendReportRequest(paymentDTO);

        ReportDTO response = futureWrapper.join(future);

        if (response != null && isSend){
            return new DTUPayResponseImpl(response);
        }
        return ResponseMessageHelper.CreateErrorResponse("Something went wrong when generating the report");
    }

    @Override
    protected ReportDTO handleMessage(String message) {
        return new Gson().fromJson(message, ReportDTO.class);
    }

    @Override
    public void receive(ReportDTO reportDTO) {
        future.complete(reportDTO);
    }

    /**
     * Try to send the report generation request
     * @param requestDTO The report generation request DTO
     * @return Boolean indicating if the request was successful
     */
    private boolean trySendReportRequest(ReportGenerationRequestDTO requestDTO){
        try {
            customerReportMessageSender.sendMessage(requestDTO, sendRoute,
                    ExchangeProvider.getReportingExchange());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
