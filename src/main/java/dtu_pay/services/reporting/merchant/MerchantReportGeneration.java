package dtu_pay.services.reporting.merchant;

import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.reporting.DTO.ReportDTO;

/**
 * Merchant Report Generation handler interface
 * @author Mathias
 */
public interface MerchantReportGeneration {

    /**
     * Generate a new Merchant Report
     * @param customerId The merchant id
     * @param to The to date
     * @param from The from date
     * @return The generated report
     */
    DTUPayResponse<ReportDTO> generateMerchantReport(String customerId, String to, String from);
}
