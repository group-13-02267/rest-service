package dtu_pay.services.reporting.merchant;

import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.helpers.FutureWrapper;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.ReportingGenerationServiceBase;

import java.util.concurrent.CompletableFuture;

/**
 * Service for generating Merchant Reports
 * @author Mathias
 */
public class MerchantReportGenerationService extends ReportingGenerationServiceBase implements MerchantReportGeneration {

    /**
     * Instantiate a new MerchantReportGenerationService
     * @param requestId Unique request id
     * @param receiveRoute The route to receive on
     * @param sendRoute The route to send on
     * @param customerReportMessageSender The message sender
     * @param futureWrapper A {@link CompletableFuture} wrapper
     */
    public MerchantReportGenerationService(String requestId, String receiveRoute, String sendRoute,
                                           MessageSender customerReportMessageSender, FutureWrapper futureWrapper) {
        super(requestId, receiveRoute, sendRoute, customerReportMessageSender, futureWrapper);
    }

    @Override
    public DTUPayResponse<ReportDTO> generateMerchantReport(String customerId, String from, String to) {
        return this.generateReport(customerId, from, to);
    }
}
