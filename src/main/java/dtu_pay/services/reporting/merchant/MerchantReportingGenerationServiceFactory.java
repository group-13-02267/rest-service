package dtu_pay.services.reporting.merchant;

import dtu_pay.common.MessageSender;
import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.ServiceFactoryBase;
import dtu_pay.services.helpers.DTUFutureWrapper;
import dtu_pay.services.reporting.ReportingGenerationServiceBase;

import java.util.UUID;

/**
 * Merchant Report Generation Service Factory
 * @author Mathias
 */
public class MerchantReportingGenerationServiceFactory extends ServiceFactoryBase<ReportingGenerationServiceBase>{

    /**
     * Generate a new Merchant Report Generation Async Service
     * @return The new {@link MerchantReportGenerationService}
     */
    @Override
    protected MerchantReportGenerationService createService() {
        String requestId = UUID.randomUUID().toString();
        String senderRoute = "reporting.merchant.request." + requestId;
        String receiverRoute = "report.generated." + requestId;
        MessageSender messageSender = new RabbitMqMessageService();
        return new MerchantReportGenerationService(requestId, receiverRoute, senderRoute, messageSender,
                                                   new DTUFutureWrapper());
    }
}
