package dtu_pay.services.reporting.customer;

import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.reporting.DTO.ReportDTO;

/**
 * Customer Report Generation Handler Interface
 * @author Mathias
 */

public interface CustomerReportGeneration {
    /**
     * Generate a new customer report
     * @param customerId The customer id
     * @param to The to date
     * @param from The from date
     * @return A response containing the report
     */
    DTUPayResponse<ReportDTO> generateCustomerReport(String customerId, String to, String from);
}
