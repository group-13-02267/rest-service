package dtu_pay.services.reporting.customer;

import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.helpers.FutureWrapper;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.ReportingGenerationServiceBase;

import java.util.concurrent.CompletableFuture;

/**
 * Implementation of Customer {@link CustomerReportGeneration}
 * @author Mathias
 */
public class CustomerReportGenerationService extends ReportingGenerationServiceBase implements CustomerReportGeneration{

    /**
     * Instantiate a new Customer Report Generation Service
     * @param requestId The unique request id
     * @param receiveRoute The route to receive on
     * @param sendRoute The route to send on
     * @param messageSender The message sender
     * @param futureWrapper A {@link CompletableFuture} wrapper
     */
    public CustomerReportGenerationService(String requestId, String receiveRoute, String sendRoute,
                                           MessageSender messageSender, FutureWrapper futureWrapper) {
        super(requestId, receiveRoute, sendRoute, messageSender, futureWrapper);
    }

    @Override
    public DTUPayResponse<ReportDTO> generateCustomerReport(String customerId, String to, String from) {
        return this.generateReport(customerId, to, from);
    }
}
