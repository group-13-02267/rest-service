package dtu_pay.services.reporting.customer;

import dtu_pay.common.MessageSender;
import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.ServiceFactoryBase;
import dtu_pay.services.helpers.DTUFutureWrapper;

import java.util.UUID;

/**
 * {@link CustomerReportGenerationService} Factory
 * @author Mathias
 */
public class CustomerReportingGenerationServiceFactory extends ServiceFactoryBase<CustomerReportGenerationService> {
    @Override
    protected CustomerReportGenerationService createService() {
        String requestId = UUID.randomUUID().toString();
        String senderRoute = "reporting.customer.request." + requestId;
        String receiverRoute = "report.generated." + requestId;
        MessageSender messageSender = new RabbitMqMessageService();
        return new CustomerReportGenerationService(requestId, receiverRoute, senderRoute,
                messageSender, new DTUFutureWrapper());
    }
}
