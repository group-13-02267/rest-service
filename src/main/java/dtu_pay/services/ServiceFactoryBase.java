package dtu_pay.services;

import core.AsyncException;
import core.AsyncService;

/**
 * Base abstract class that all Service Factory for Async Services must inherit
 * @param <T>'
 * @author Mathias
 */
public abstract class ServiceFactoryBase<T extends AsyncService> implements ServiceFactory {

    @Override
    public  T setupService() {
        T service = null;
        try {
            service = createService();
            service.listen();
        } catch (AsyncException ex) {
            ex.printStackTrace();
        }
        return service;
    }

    /**
     * Create the {@link AsyncService}
     * @return The new {@link AsyncService}
     */
    protected abstract T createService();
}
