package dtu_pay.services;

import core.AsyncSender;
import core.RabbitSender;
import dtu_pay.common.MessageSender;

/**
 * {@link MessageSender} implementation for sending messages over RabbitMQ
 */
public class RabbitMqMessageService implements MessageSender {
    @Override
    public void sendMessage(Object content, String route, String exchange) {
        AsyncSender eventSender = new RabbitSender(exchange, route);
        eventSender.send(content);
    }
}
