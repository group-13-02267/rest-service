package dtu_pay.services;

/**
 * Helper class for returning the correct exchanges
 * @author Mathias
 */
public class ExchangeProvider {
    public static String getReportingExchange(){
        return "reporting";
    }

    public static String getPaymentExchange(){
        return "payment";
    }
    public static String getAdministrationExchange(){
        return "administration";
    }
    public static String getTokenExchange() { return "token";}

}
