package dtu_pay.services;

/**
 * Service Factory Interface for setting up services
 * @author Mathias
 */
public interface ServiceFactory<T> {
    /**
     * Setup the service
     * @return The service that was setup
     */
    T setupService();
}
