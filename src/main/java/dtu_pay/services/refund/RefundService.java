package dtu_pay.services.refund;

import dtu_pay.rest.core.DTUPayEmptyResponse;

/**
 * Interface handling refunds
 */
public interface RefundService {
    /**
     * Refund a payment
     * @param token The token used for the refund
     * @param merchantId The merchant id
     * @param amount The refund amount
     * @return Empty response with the refund status
     */
    DTUPayEmptyResponse refundPayment(String token, String merchantId, float amount);
}
