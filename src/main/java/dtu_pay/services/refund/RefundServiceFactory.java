package dtu_pay.services.refund;

import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.ServiceFactoryBase;

import java.util.UUID;

/**
 * Refund Service Factory
 * @author Mathias Ibsen
 */
public class RefundServiceFactory extends ServiceFactoryBase<DTURefundService> {

    /**
     * Create a new Refund Service with a new unique request id and a {@link RabbitMqMessageService}
     * @return The new {@link DTURefundService}
     */
    @Override
    protected DTURefundService createService() {
        return new DTURefundService(UUID.randomUUID().toString(), new RabbitMqMessageService());
    }
}
