package dtu_pay.services.refund;

import com.google.gson.Gson;
import core.AsyncService;
import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayEmptyResponseImpl;
import dtu_pay.services.ExchangeProvider;
import dtu_pay.services.helpers.ResponseMessageHelper;
import dtu_pay.services.payment.DTO.ResponseDTO;
import dtu_pay.services.refund.DTO.RefundDTO;

import java.util.concurrent.CompletableFuture;

/**
 * Refund Service implementation of {@link RefundService} and is implemented as an {@link AsyncService} to listen
 * for the refund response.
 */
public class DTURefundService extends AsyncService<ResponseDTO> implements RefundService {

    /**
     * The future used for receiving the refund response
     */
    private CompletableFuture<ResponseDTO> future;

    /**
     * The unique request id
     */
    String tokenRequestId;

    /**
     * The message sender
     */
    private final MessageSender messageSenderService;

    /**
     * Instantiate a new Refund Service
     * @param refundRequestId The unique request id
     * @param messageSenderService The message sender
     */
    public DTURefundService(String refundRequestId, MessageSender messageSenderService) {
        super(ExchangeProvider.getPaymentExchange(), "transaction.refund.response." + refundRequestId);
        tokenRequestId = refundRequestId;
        this.messageSenderService = messageSenderService;
    }

    @Override
    protected ResponseDTO handleMessage(String message) {
        return new Gson().fromJson(message, ResponseDTO.class);
    }

    @Override
    public void receive(ResponseDTO received) {
        future.complete(received);
    }

    @Override
    public DTUPayEmptyResponse refundPayment(String token, String merchantId, float amount) {
        RefundDTO refundDTO = new RefundDTO(tokenRequestId, token, merchantId, amount);
        future = new CompletableFuture<>();

        boolean isSend = trySendRefund(refundDTO);

        ResponseDTO response = future.join();

        if (response.success && isSend){
            return new DTUPayEmptyResponseImpl();
        }
        return ResponseMessageHelper.CreateEmptyErrorResponse("Something went wrong when transferring the money");
    }

    /**
     * Try and send the refund
     * @param payment The refund DTO
     * @return Boolean indicating if the refund was successful or not
     */
    private boolean trySendRefund(RefundDTO payment){
        try {
            messageSenderService.sendMessage(payment, "transaction.refund.received." + tokenRequestId,
                    ExchangeProvider.getPaymentExchange());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
