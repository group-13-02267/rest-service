package dtu_pay.services.refund.DTO;

/**
 * DTO for Refund
 */
public class RefundDTO {

    /**
     * Instantiate a new RefundDTO
     * @param id The unique refund id
     * @param token The token used for the refund
     * @param sender The refund sender
     * @param amount The refund amount
     */
    public RefundDTO(String id, String token, String sender, float amount) {
        this.id = id;
        this.token = token;
        this.sender = sender;
        this.amount = amount;
    }

    public RefundDTO() {
    }

    /**
     * The unique refund id
     */
    public String id;

    /**
     * The token used for the refund
     */
    public String token;

    /**
     * The refund sender
     */
    public String sender;

    /**
     * The refund amount
     */
    public float amount;
}
