package dtu_pay.services.token.DTO;

/**
 * DTO for requesting new tokens
 */
public class TokenRequestDTO {

    /**
     * The amount of tokens to be requested
     */
    public int amount;

    /**
     * Instantiate a new TokenRequestDTO
     * @param amount The amount of tokens to request
     */
    public TokenRequestDTO(int amount) {
        this.amount = amount;
    }

    public TokenRequestDTO() {

    }
}
