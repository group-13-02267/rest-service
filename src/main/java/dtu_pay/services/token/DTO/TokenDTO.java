package dtu_pay.services.token.DTO;

/**
 * Token DTO for making sure that only token value gets send
 */
public class TokenDTO {

    /**
     * The raw token value
     */
    public String value;
}
