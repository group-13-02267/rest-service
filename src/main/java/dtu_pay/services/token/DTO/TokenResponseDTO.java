package dtu_pay.services.token.DTO;

import java.util.List;

/**
 * Token Request Response DTO
 */
public class TokenResponseDTO {

    /**
     * The unique request id
     */
    public String id;

    /**
     * A list of the generated tokens if status is true, otherwise an empty list
     */
    public List<TokenDTO> tokens;

    /**
     * Status indicating if the generation was successful
     */
    public boolean status;
}
