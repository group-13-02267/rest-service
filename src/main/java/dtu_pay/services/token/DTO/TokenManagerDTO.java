package dtu_pay.services.token.DTO;

/**
 * DTO for requesting new tokens
 */
public class TokenManagerDTO {

    /**
     * The amount of tokens to be requested
     */
    public int amount;

    /**
     * The customer id requesting the tokens
     */
    public String customer;

    /**
     * The unique request id
     */
    public String id;

    /**
     * Instantiate a new TokenRequestDTO
     * @param id The unique request id
     * @param customer The customer id
     * @param amount The amount of tokens to request
     */
    public TokenManagerDTO(String id, String customer, int amount) {
        this.amount = amount;
        this.customer = customer;
        this.id = id;
    }

    public TokenManagerDTO() {

    }
}
