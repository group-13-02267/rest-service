package dtu_pay.services.token;

import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.ServiceFactoryBase;

import java.util.UUID;

/**
 * Factory for generating {@link DTUTokenService}
 * @author Mathias
 */
public class TokenServiceFactory extends ServiceFactoryBase<DTUTokenService> {

    /**
     * Create a new {@link DTUTokenService} with a unique request id and {@link RabbitMqMessageService}
     * @return The new  {@link DTUTokenService}
     */
    @Override
    protected DTUTokenService createService() {
        return new DTUTokenService(UUID.randomUUID().toString(), new RabbitMqMessageService());
    }
}
