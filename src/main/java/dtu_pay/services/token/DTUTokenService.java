package dtu_pay.services.token;

import com.google.gson.Gson;
import core.AsyncService;
import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.rest.core.DTUPayResponseImpl;
import dtu_pay.services.ExchangeProvider;
import dtu_pay.services.helpers.ResponseMessageHelper;
import dtu_pay.services.token.DTO.TokenManagerDTO;
import dtu_pay.services.token.DTO.TokenResponseDTO;

import java.util.concurrent.CompletableFuture;

/**
 * Token Service implemented as an AsyncService that generates new tokens
 * @author Mathias
 */
public class DTUTokenService extends AsyncService<TokenResponseDTO> implements TokenService {

    /**
     * The unique request id
     */
    private String tokenRequestId;

    /**
     * The future that receives the response
     */
    private CompletableFuture<TokenResponseDTO> future;

    /**
     * The message sender
     */
    private final MessageSender tokenMessageHandler;

    /**
     * Instantiate a new DTUTokenService
     * @param tokenRequestId The unique request id
     * @param tokenMessageHandler The message handler
     */
    public DTUTokenService(String tokenRequestId, MessageSender tokenMessageHandler) {
        super(ExchangeProvider.getTokenExchange(), "token.generated." + tokenRequestId);
        this.tokenRequestId = tokenRequestId;
        this.tokenMessageHandler = tokenMessageHandler;
    }

    @Override
    public DTUPayResponse<TokenResponseDTO> requestTokens(String user, int count) {
        TokenManagerDTO requestDTO = new TokenManagerDTO(tokenRequestId, user, count);
        future = new CompletableFuture<>();

        boolean isSend = trySendTokenRequest(requestDTO);

        TokenResponseDTO response = future.join();

        if (response != null && response.status && isSend){
            return new DTUPayResponseImpl(response);
        }
        return ResponseMessageHelper.CreateErrorResponse("Could not generate the requested amount of tokens");
    }

    @Override
    protected TokenResponseDTO handleMessage(String message) {
        return new Gson().fromJson(message, TokenResponseDTO.class);
    }

    @Override
    public void receive(TokenResponseDTO tokenResponseDTO) {
        future.complete(tokenResponseDTO);
    }

    /**
     * Try and send new token request
     * @param tokenRequest The token request DTO
     * @return Boolean representing the generation status
     */
    private boolean trySendTokenRequest(TokenManagerDTO tokenRequest){
        try {
            tokenMessageHandler.sendMessage(tokenRequest, "token.request." + tokenRequestId, ExchangeProvider.getTokenExchange());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
