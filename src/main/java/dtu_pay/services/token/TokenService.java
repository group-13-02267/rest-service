package dtu_pay.services.token;

import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.token.DTO.TokenResponseDTO;

/**
 * Interface that all token services must inherit from
 * @author Mathias
 */
public interface TokenService {
    /**
     * Request new tokens
     * @param user The user id
     * @param count The amount of tokens requested
     * @return A response with the requested tokens
     */
    DTUPayResponse<TokenResponseDTO> requestTokens(String user, int count);
}
