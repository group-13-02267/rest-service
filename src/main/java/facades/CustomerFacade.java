package facades;

import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.token.DTO.TokenResponseDTO;

import java.util.List;

/**
 * Interface that all customer facades implementations must inherit from
 */
public interface CustomerFacade {

    /**
     * Generate a new report
     * @param merchantId The merchant id to generate report for
     * @param from The from date
     * @param to The to date
     * @return A response with the report
     */
    DTUPayResponse<ReportDTO> generateReport(String merchantId, String from, String to);

    /**
     * Create a new customer
     * @param firstName Customer first name
     * @param lastName Customer last name
     * @param cpr Customer cpr
     * @return An empty response with the creation status
     */
	DTUPayEmptyResponse createCustomer(String firstName, String lastName, String cpr);

    /**
     * Generate new tokens
     * @param customerId The customer that wants new tokens
     * @param amount The amount of tokens to generate
     * @return A response with the generated tokens if successful, otherwise an error message
     */
    DTUPayResponse<TokenResponseDTO> generateTokens(String customerId, int amount);
}
