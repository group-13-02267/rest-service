package facades;

import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.ServiceFactory;
import dtu_pay.services.administration.AdministrationService;
import dtu_pay.services.administration.AdministrationServiceFactory;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.ReportingGeneration;
import dtu_pay.services.reporting.ReportingGenerationService;
import dtu_pay.services.reporting.customer.CustomerReportingGenerationServiceFactory;
import dtu_pay.services.reporting.merchant.MerchantReportingGenerationServiceFactory;
import dtu_pay.services.token.DTO.TokenResponseDTO;
import dtu_pay.services.token.DTUTokenService;
import dtu_pay.services.token.TokenService;
import dtu_pay.services.token.TokenServiceFactory;

/**
 * {@link CustomerFacade} implementation for creating new customers, reports and tokens
 */
public class DTUCustomerFacade implements CustomerFacade {

    private final ReportingGeneration reportingGeneration;
    ServiceFactory<AdministrationService> administrationServiceFactory;
    private final ServiceFactory<TokenService> tokenServiceFactory;
    
    public DTUCustomerFacade(ReportingGeneration reportingGeneration, ServiceFactory<TokenService> tokenServiceFactory, ServiceFactory<AdministrationService> administrationServiceFactory) {
        this.reportingGeneration = reportingGeneration;
        this.tokenServiceFactory = tokenServiceFactory;
        this.administrationServiceFactory = administrationServiceFactory;
    }

    /**
     * Create a new Customer Facade
     * @return The new customer facade
     */
    public static CustomerFacade create(){
        return new DTUCustomerFacade(new ReportingGenerationService(
                new MerchantReportingGenerationServiceFactory(), new CustomerReportingGenerationServiceFactory()),new TokenServiceFactory(),new AdministrationServiceFactory("customer"));
    }

    @Override
    public DTUPayEmptyResponse createCustomer(String firstName, String lastName, String cpr) {
        return administrationServiceFactory.setupService().createCustomer(firstName,lastName,cpr);
    }
    
    @Override
    public DTUPayResponse<ReportDTO> generateReport(String customerId, String from, String to) {
        return reportingGeneration.generateCustomerReport(customerId, from, to);
    }

    @Override
    public DTUPayResponse<TokenResponseDTO> generateTokens(String customerId, int amount) {
        return tokenServiceFactory.setupService().requestTokens(customerId, amount);
    }
}
