package facades;

import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.reporting.DTO.ReportDTO;

/**
 * Merchant Facade interface that all Merchant Facade implements must inherit from
 * @author Mathias Ibsens
 */
public interface MerchantFacade {
    /**
     * Create a new merchant
     * @param firstName The merchant first name
     * @param lastName The merchant last name
     * @param cpr The merchant cpr
     * @return An empty response indicating the creation status
     */
    DTUPayEmptyResponse createMerchant(String firstName, String lastName, String cpr);

    /**
     * Generate a merchant report
     * @param merchantId The merchant id
     * @param from The from date
     * @param to The to date
     * @return A response with the report
     */
    DTUPayResponse<ReportDTO> generateReport(String merchantId, String from, String to);

    /**
     * Refund a payment
     * @param token The token used to refund
     * @param merchant_cpr The merchant CPR
     * @param amount The amount to refund
     * @return An empty response indicating the status
     */
    DTUPayEmptyResponse refundPayment(String token, String merchant_cpr, float amount);

    /**
     * Create a payment
     * @param token The token used for the payment
     * @param merchant_cpr The merchant CPR
     * @param amount The amount to be payed
     * @return An empty response indicating the status
     */
    DTUPayEmptyResponse createPayment(String token, String merchant_cpr, float amount);
}
