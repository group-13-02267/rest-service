package facades;

import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.ServiceFactory;
import dtu_pay.services.administration.AdministrationService;
import dtu_pay.services.administration.AdministrationServiceFactory;
import dtu_pay.services.payment.PaymentService;
import dtu_pay.services.payment.PaymentServiceFactory;
import dtu_pay.services.refund.RefundService;
import dtu_pay.services.refund.RefundServiceFactory;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.ReportingGeneration;
import dtu_pay.services.reporting.ReportingGenerationService;
import dtu_pay.services.reporting.customer.CustomerReportingGenerationServiceFactory;
import dtu_pay.services.reporting.merchant.MerchantReportingGenerationServiceFactory;

/**
 * {@link MerchantFacade} implementation that can create new merchants, generate reports and refund payments
 * @author Mathias Ibsens
 */
public class DTUMerchantFacade implements MerchantFacade {

    ServiceFactory<PaymentService> paymentServiceFactory;
    ReportingGeneration reportingGeneration;
    private final ServiceFactory<RefundService> refundServiceServiceFactory;
    ServiceFactory<AdministrationService> administrationServiceFactory;

    public DTUMerchantFacade(ServiceFactory<PaymentService> paymentServiceFactory,
                             ServiceFactory<RefundService> refundServiceServiceFactory,
                             ReportingGeneration reportingGeneration,
                             ServiceFactory<AdministrationService> administrationServiceFactory) {
        this.paymentServiceFactory = paymentServiceFactory;
        this.reportingGeneration = reportingGeneration;
        this.refundServiceServiceFactory = refundServiceServiceFactory;
        this.administrationServiceFactory = administrationServiceFactory;
    }

    @Override
    public DTUPayEmptyResponse createMerchant(String firstName, String lastName, String cpr) {
        return administrationServiceFactory.setupService().createMerchant(firstName,lastName,cpr);
    }

    @Override
    public DTUPayResponse<ReportDTO> generateReport(String merchantId, String from, String to) {
        return reportingGeneration.generateMerchantReport(merchantId, from, to);
    }

    @Override
    public DTUPayEmptyResponse refundPayment(String token, String merchant_cpr, float amount) {
        return refundServiceServiceFactory.setupService().refundPayment(token, merchant_cpr, amount);
    }

    @Override
    public DTUPayEmptyResponse createPayment(String token, String merchant_cpr, float amount) {
        return paymentServiceFactory.setupService().createPayment(token, merchant_cpr, amount);
    }

    /**
     * Create a new merchant facade
     * @return The new merchant facade
     */
    public static MerchantFacade create(){
        return new DTUMerchantFacade(new PaymentServiceFactory(), new RefundServiceFactory(), new ReportingGenerationService(
                new MerchantReportingGenerationServiceFactory(), new CustomerReportingGenerationServiceFactory()
        ),new AdministrationServiceFactory("merchant"));
    }
}
