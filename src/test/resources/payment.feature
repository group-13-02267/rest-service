Feature: Payment feature

  Scenario: Successful payment
    Given a customer with a token
    And a merchant with an account
    When the first customer makes a valid payment to the merchant
    Then the payment request event is broadcast
    When the payment is received
    Then the payment request succeeds

  Scenario: Successful refund
    Given a customer with 2 tokens
    And a merchant with an account
    When the first customer makes a valid payment to the merchant
    And the merchant makes a refund to the first customer
    Then the refund request is broadcast
    When the refund is received
    Then the refund request succeeds
