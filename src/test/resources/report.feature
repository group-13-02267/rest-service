Feature: Report feature
  Scenario: Requesting a customer report
    Given a customer with a token
    And a merchant with an account
    When the first customer makes a valid payment to the merchant
    Then the customer request a report
    Then the report request event is broadcast
    Then a report with the payment is received