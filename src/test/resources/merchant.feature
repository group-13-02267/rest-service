Feature: creating merchants
  Scenario: Creating a valid merchant
    When a request for a new valid merchant is received
    Then the merchant request event is broadcast
    And an event response is received
    And the merchant request succeeds

  Scenario: Creating an invalid merchant
    Given an existing merchant
    When a request for a merchant with a duplicate cpr is received
    Then the merchant request event is broadcast
    And an event response is received
    And an unsuccessful response is returned
