Feature: Customer Resource Feature

  Scenario: Creating Customer
    Given a customer creation request is made
    When Creating the customer
    Then Return successful response
    And The customer should be created

  Scenario: Creating Invalid Customer
    Given a customer creation request is received
    And a customer creation request with the same cpr is received
    When Creating a customer with the same cpr
    Then Return unsuccessful response
