package unittests.facades;

import dtu_pay.services.ServiceFactory;
import dtu_pay.services.administration.AdministrationService;
import dtu_pay.services.payment.PaymentService;
import dtu_pay.services.refund.RefundService;
import dtu_pay.services.reporting.ReportingGeneration;
import facades.CustomerFacade;
import facades.DTUCustomerFacade;
import facades.DTUMerchantFacade;
import facades.MerchantFacade;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class DTUMerchantFacadeTest {

    private ReportingGeneration reportingGeneration;
    private MerchantFacade merchantFacade;
    private ServiceFactory<PaymentService> paymentServiceFactory;
    private ServiceFactory<AdministrationService> administrationServiceFactory;
    private ServiceFactory<RefundService> refundServiceServiceFactory;

    @Before
    public void setup(){
        reportingGeneration = mock(ReportingGeneration.class);
        paymentServiceFactory =  (ServiceFactory<PaymentService>) mock(ServiceFactory.class);
        administrationServiceFactory=(ServiceFactory<AdministrationService>) mock(ServiceFactory.class);
        refundServiceServiceFactory = (ServiceFactory<RefundService>) mock(ServiceFactory.class);
        merchantFacade = new DTUMerchantFacade(paymentServiceFactory, refundServiceServiceFactory, reportingGeneration,administrationServiceFactory);
    }


    @Test
    public void generateReport_whenCalled_expectReportServiceIsInvokedWithCorrectParameters(){
        String mrechantId = "mrechantId";
        String from = "from";
        String to = "to";
        merchantFacade.generateReport(mrechantId, from, to);
        verify(reportingGeneration).generateMerchantReport(mrechantId, from, to);
    }

    @Test
    public void createPayment_whenCalled_expectPaymentServiceIsCalledWithCorrectParameters(){
        String token = "token";
        String merchant_cpr = "merchant_cpr";
        float amount = 5;
        PaymentService paymentService = mock(PaymentService.class);

        when(paymentServiceFactory.setupService()).thenReturn(paymentService);
        merchantFacade.createPayment(token, merchant_cpr, amount);
        verify(paymentService).createPayment(token, merchant_cpr, amount);
    }
    
    @Test
    public void createMerchant_whenCalled_expectAdministrationServiceIsInvokedWithCorrectParameters(){
        String firstName="First";
        String lastName="Last";
        String cpr = "cpr_number";
        AdministrationService administrationService=mock(AdministrationService.class);
        
        when(administrationServiceFactory.setupService()).thenReturn(administrationService);
        merchantFacade.createMerchant(firstName, lastName, cpr);
        verify(administrationService).createMerchant(firstName, lastName, cpr);
    }

}
