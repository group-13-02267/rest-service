package unittests.facades;

import dtu_pay.services.ServiceFactory;
import dtu_pay.services.administration.AdministrationService;
import dtu_pay.services.reporting.ReportingGeneration;
import dtu_pay.services.token.DTUTokenService;
import dtu_pay.services.token.TokenService;
import facades.CustomerFacade;
import facades.DTUCustomerFacade;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DTUCostomerFacadeTest {

    private ReportingGeneration reportingGeneration;
    private CustomerFacade customerFacade;
    private ServiceFactory<AdministrationService> administrationServiceFactory;
    private ServiceFactory<TokenService> tokenServiceServiceFactory;

    @Before
    public void setup(){
        reportingGeneration = mock(ReportingGeneration.class);
        administrationServiceFactory=(ServiceFactory<AdministrationService>) mock(ServiceFactory.class);
        tokenServiceServiceFactory = (ServiceFactory<TokenService>) mock(ServiceFactory.class);
        customerFacade = new DTUCustomerFacade(reportingGeneration, tokenServiceServiceFactory ,administrationServiceFactory);
    }


    @Test
    public void generateReport_whenCalled_expectReportServiceIsInvokedWithCorrectParameters(){
        String customerId = "customerId";
        String from = "from";
        String to = "to";

        customerFacade.generateReport(customerId, from, to);
        verify(reportingGeneration).generateCustomerReport(customerId, from, to);

    }

    @Test
    public void generateTokens_whenCalled_expectTokenServiceIsInvokedWithCorrectParameters(){
        String customerId = "customerId";
        int amount = 10;

        TokenService tokenService = mock(TokenService.class);
        when(tokenServiceServiceFactory.setupService()).thenReturn(tokenService);
        customerFacade.generateTokens(customerId, amount);
        verify(tokenService).requestTokens(customerId, amount);
    }

    
    @Test
    public void createCustomer_whenCalled_expectAdministrationServiceIsInvokedWithCorrectParameters(){
        String firstName="First";
        String lastName="Last";
        String cpr = "cpr_number";
        AdministrationService administrationService=mock(AdministrationService.class);
        
        when(administrationServiceFactory.setupService()).thenReturn(administrationService);
        customerFacade.createCustomer(firstName, lastName, cpr);
        verify(administrationService).createCustomer(firstName, lastName, cpr);
    }

}
