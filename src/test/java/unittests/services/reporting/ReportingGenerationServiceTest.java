package unittests.services.reporting;

import dtu_pay.services.ServiceFactory;
import dtu_pay.services.reporting.ReportingGenerationService;
import dtu_pay.services.reporting.customer.CustomerReportGeneration;
import dtu_pay.services.reporting.merchant.MerchantReportGeneration;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class ReportingGenerationServiceTest {

    private ServiceFactory<MerchantReportGeneration> merchantReportServiceFactory;
    private ServiceFactory<CustomerReportGeneration> customerReportGenerationServiceFactory;
    private ReportingGenerationService reportingGenerationService;

    @Before
    public void setup(){
        merchantReportServiceFactory = (ServiceFactory<MerchantReportGeneration>) mock(ServiceFactory.class);
        customerReportGenerationServiceFactory = (ServiceFactory<CustomerReportGeneration>) mock(ServiceFactory.class);
        reportingGenerationService = new ReportingGenerationService(merchantReportServiceFactory, customerReportGenerationServiceFactory);
    }

    @Test
    public void generateCustomerReport_whenInvoked_expectCustomerReportingServiceIsInvokedWithCorrectParameters(){
        //setup
        String customerId = "customerId";
        String from = "from";
        String to = "to";
        CustomerReportGeneration customerReportGeneration = mock(CustomerReportGeneration.class);
        when(customerReportGenerationServiceFactory.setupService()).thenReturn(customerReportGeneration);
        //act
        reportingGenerationService.generateCustomerReport(customerId, from, to);
        //assert
        verify(customerReportGeneration).generateCustomerReport(customerId,from, to);
    }

    @Test
    public void generateMerchantReport_whenInvoked_expectCustomerReportingServiceIsInvokedWithCorrectParameters(){
        //setup
        String merchantId = "merchantId";
        String from = "from";
        String to = "to";
        MerchantReportGeneration merchantReportGeneration = mock(MerchantReportGeneration.class);
        when(merchantReportServiceFactory.setupService()).thenReturn(merchantReportGeneration);
        //act
        reportingGenerationService.generateMerchantReport(merchantId, from, to);
        //assert
        verify(merchantReportGeneration).generateMerchantReport(merchantId,from, to);
    }
}
