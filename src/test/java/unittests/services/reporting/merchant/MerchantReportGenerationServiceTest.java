package unittests.services.reporting.merchant;
import dtu_pay.common.MessageSender;
import dtu_pay.services.helpers.FutureWrapper;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.merchant.MerchantReportGenerationService;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class MerchantReportGenerationServiceTest {

    @Test
    public void generateMerchantReport_whenInvoked_expectCorrectMessageSend(){
        String requestId = "requestId";
        String receiveRoute = "receiveRoute";
        String sendRoute = "sendRoute";
        MessageSender messageSender = mock(MessageSender.class);
        FutureWrapper futureWrapper = mock(FutureWrapper.class);
        MerchantReportGenerationService merchantReportGenerationService =
                new MerchantReportGenerationService(requestId, receiveRoute, sendRoute, messageSender, futureWrapper);
        when(futureWrapper.join(any(CompletableFuture.class))).thenReturn(new ReportDTO());

        merchantReportGenerationService.generateMerchantReport("test", "hest", "to");

        merchantReportGenerationService.receive(new ReportDTO());
        verify(messageSender).sendMessage(any(Object.class), eq(sendRoute), eq("reporting"));
    }
}
