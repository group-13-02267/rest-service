package unittests.services.administration;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.contains;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

import dtu_pay.common.MessageSender;
import dtu_pay.services.ServiceFactory;
import dtu_pay.services.administration.AdministrationService;
import dtu_pay.services.administration.AdministrationServiceImpl;
import dtu_pay.services.helpers.FutureWrapper;
import dtu_pay.services.payment.DTO.ResponseDTO;
import dtu_pay.services.reporting.DTO.ReportDTO;


public class AdministrationServiceTest {

	private ServiceFactory<AdministrationService> administrationServiceFactory;
	private AdministrationServiceImpl customerAdministrationServiceImpl;
	private AdministrationServiceImpl merchantAdministrationServiceImpl;
	MessageSender messageSender; 
	CompletableFuture<ResponseDTO> future;

	@Before
	public void setup() {
		messageSender= mock(MessageSender.class);
		future=mock(CompletableFuture.class);
		administrationServiceFactory=(ServiceFactory<AdministrationService>) mock(ServiceFactory.class);
		when(future.join()).thenReturn(new ResponseDTO());
		customerAdministrationServiceImpl=new AdministrationServiceImpl("customer", "1347389423",messageSender,future);
		merchantAdministrationServiceImpl=new AdministrationServiceImpl("merchant", "5345345345",messageSender,future);
	}

	@Test
	public void createCustomer_whenInvoked_expectCorrectMessageSend(){
		  String firstName = "First";
	        String lastName = "Last";
	        String cpr = "testcpr121";
	        
	        
	        
	        when(administrationServiceFactory.setupService()).thenReturn(customerAdministrationServiceImpl);
	        customerAdministrationServiceImpl.createCustomer(firstName, lastName, cpr);
	        verify(messageSender).sendMessage(any(Object.class), contains("administration.customer.create."), eq("administration"));
	}
	@Test
	public void createMerchant_whenInvoked_expectCorrectMessageSend(){
		  String firstName = "First";
	        String lastName = "Last";
	        String cpr = "testcpr121";
	        
	        
	        when(administrationServiceFactory.setupService()).thenReturn(merchantAdministrationServiceImpl);
	        merchantAdministrationServiceImpl.createMerchant(firstName, lastName, cpr);
	        verify(messageSender).sendMessage(any(Object.class), contains("administration.merchant.create."), eq("administration"));
	}
}
