package unittests.services;

import dtu_pay.services.ExchangeProvider;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExchangeProviderTest {
    @Test
    public void sendMessage(){
        assertEquals("reporting" ,ExchangeProvider.getReportingExchange());
        assertEquals("payment" ,ExchangeProvider.getPaymentExchange());
    }

}
