package servicetests;

import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.payment.DTO.PaymentDTO;
import dtu_pay.services.payment.DTO.ResponseDTO;
import dtu_pay.services.payment.PaymentServiceImpl;
import dtu_pay.services.refund.DTO.RefundDTO;
import dtu_pay.services.refund.DTURefundService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

public class PaymentSteps {

    List<String> validCustomerTokens = new ArrayList<>();
    String lastUsedToken;
    String merchant;

    PaymentServiceImpl paymentService;
    DTURefundService refundService;
    MessageSender paymentMessageSender;
    MessageSender refundMessageSender;
    CompletableFuture<DTUPayEmptyResponse> response = new CompletableFuture<>();
    String paymentId = UUID.randomUUID().toString();
    String refundId = UUID.randomUUID().toString();
    CompletableFuture<PaymentDTO> paymentFeature = new CompletableFuture<>();
    CompletableFuture<RefundDTO> refundFeature = new CompletableFuture<>();
    float amount;

    public PaymentSteps() {
        paymentMessageSender = new RabbitMqMessageService() {
            @Override
            public void sendMessage(Object content, String route, String exchange) {
                paymentFeature.complete((PaymentDTO) content);
                Assert.assertEquals("payment", exchange);
                Assert.assertEquals("transaction.payment.received." + paymentId, route);
            }
        };

        paymentService = new PaymentServiceImpl(paymentId, paymentMessageSender);
        refundMessageSender = new RabbitMqMessageService() {
            @Override
            public void sendMessage(Object content, String route, String exchange) {
                RefundDTO refundDTO = (RefundDTO) content;
                refundDTO.sender = "some_customer";
                refundFeature.complete(refundDTO);
                assertEquals("payment", exchange);
                assertEquals("transaction.refund.received." + refundId, route);
            }
        };
        refundService = new DTURefundService(refundId, refundMessageSender);
    }

    @Given("a customer with a token")
    public void aCustomerWithAnAccount() {
        validCustomerTokens.add("testToken");
    }

    @Given("a customer with {int} token(s)")
    public void aCustomerWithNTokens(int amount) {
        for (int i = 0; i < amount; i++) {
            validCustomerTokens.add("testToken" + i);
        }
    }

    @Given("a merchant with an account")
    public void aMerchantWithAnAccount() {
        merchant = "merchant";
    }

    @When("the first customer makes a valid payment to the merchant")
    public void theFirstCustomerMakesAValidPaymentToTheMerchant() {
        amount = 100;
        String token = validCustomerTokens.remove(0);
        lastUsedToken = token;

        new Thread(() -> {
            try {
                response.complete(paymentService.createPayment(token, merchant, amount));
            } catch (Exception e) {
                throw new Error(e);
            }
        }).start();
    }

    @When("the merchant makes a refund to the first customer")
    public void theMerchantMakesARefundToTheFirstCustomer() {
        amount = 50;
        String token = validCustomerTokens.remove(0);
        lastUsedToken = token;

        new Thread(() -> {
            try {
                response.complete(refundService.refundPayment(token, merchant, amount));
            } catch (Exception e) {
                throw new Error(e);
            }
        }).start();
    }

    @When("the refund is received")
    public void theRefundIsReceived() {
        refundService.receive(new ResponseDTO(true));
    }

    @Then("the payment request event is broadcast")
    public void thePaymentRequestEventIsBroadcast() {
        PaymentDTO paymentDTO = paymentFeature.join();
        assertEquals(lastUsedToken, paymentDTO.token);
        assertEquals(merchant, paymentDTO.receiver);
        assertTrue(amount == paymentDTO.amount);
        assertEquals(paymentId, paymentDTO.id);
    }

    @Then("the refund request is broadcast")
    public void theRefundRequestIsBroadcast() {
        RefundDTO refundDTO = refundFeature.join();
        assertEquals(lastUsedToken, refundDTO.token);
        assertTrue(amount == 50);
        assertNotEquals(merchant, refundDTO.sender);
        assertEquals(refundId, refundDTO.id);
    }

    @When("the payment is received")
    public void thePaymentIsReceived() {
       paymentService.receive(new ResponseDTO(true));
    }

    @Then("the payment request succeeds")
    public void thePaymentRequestSucceeds() {
        assertTrue(response.join().success());
    }

    @Then("the refund request succeeds")
    public void theRefundRequestSucceeds() {
        assertTrue(response.join().success());
    }

}
