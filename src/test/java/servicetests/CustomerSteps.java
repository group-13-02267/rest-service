package servicetests;

import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.administration.AdministrationService;
import dtu_pay.services.administration.AdministrationServiceImpl;
import dtu_pay.services.administration.DTO.CustomerDTO;
import dtu_pay.services.payment.DTO.ResponseDTO;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

public class CustomerSteps {

    private final AdministrationService administrationService1;
    private final AdministrationService administrationService2;
    private final AdministrationService administrationService3;

    String customerId = UUID.randomUUID().toString();
    MessageSender customerCreationSender;
    MessageSender customerValidCreationSender;
    MessageSender customerInvalidCreationSender;
    CompletableFuture<DTUPayEmptyResponse> responseFuture = new CompletableFuture<>();
    CompletableFuture<DTUPayEmptyResponse> ValidatedResponseFuture = new CompletableFuture<>();
    CompletableFuture<DTUPayEmptyResponse> InvalidResponseFuture = new CompletableFuture<>();
    private CustomerDTO customerDTO;
    private CustomerDTO createdCustomer;
    private CustomerDTO invalidatedCreatedCustomer;
    private CompletableFuture<ResponseDTO> customerCreationFuture = new CompletableFuture<>();
    private CompletableFuture<ResponseDTO> customerValidCreationFuture = new CompletableFuture<>();
    ;
    private CompletableFuture<ResponseDTO> customerInvalidCreationFuture = new CompletableFuture<>();
    ;

    public CustomerSteps() {
        customerCreationSender = new RabbitMqMessageService() {
            @Override
            public void sendMessage(Object content, String route, String exchange) {
                createdCustomer = (CustomerDTO) content;
                customerCreationFuture.complete(new ResponseDTO(true));
                Assert.assertEquals("administration", exchange);
                Assert.assertEquals("administration.customer.create." + customerId, route);
            }
        };

        customerValidCreationSender = new RabbitMqMessageService() {
            @Override
            public void sendMessage(Object content, String route, String exchange) {
                createdCustomer = (CustomerDTO) content;
                customerValidCreationFuture.complete(new ResponseDTO(true));
                Assert.assertEquals("administration", exchange);
                Assert.assertEquals("administration.customer.create." + createdCustomer.instanceId, route);
            }
        };

        customerInvalidCreationSender = new RabbitMqMessageService() {
            @Override
            public void sendMessage(Object content, String route, String exchange) {
                invalidatedCreatedCustomer = (CustomerDTO) content;
                customerInvalidCreationFuture.complete(new ResponseDTO(false));
                Assert.assertEquals("administration", exchange);
                Assert.assertEquals("administration.customer.create." + invalidatedCreatedCustomer.instanceId, route);
            }
        };

        administrationService1 = new AdministrationServiceImpl("customer",
                customerId, customerCreationSender, customerCreationFuture);
        administrationService2 = new AdministrationServiceImpl("customer",
                customerId, customerValidCreationSender, customerValidCreationFuture);
        administrationService3 = new AdministrationServiceImpl("customer",
                customerId, customerInvalidCreationSender, customerInvalidCreationFuture);
    }

    @Given("^a customer creation request is made$")
    public void customerCreationRequest() {
        customerDTO = new CustomerDTO("first", "last", "cpr");
    }

    @When("^Creating the customer$")
    public void requestReceived() {
        new Thread(() -> responseFuture.complete(administrationService1.createCustomer(customerDTO.firstName,
                customerDTO.lastName,
                customerDTO.cpr))).start();
    }

    @Then("^Return successful response$")
    public void createCustomer() {
        DTUPayEmptyResponse response = responseFuture.join();
        assertTrue(response.success());
    }

    @And("^The customer should be created$")
    public void returnSuccessfulResponse() {
        assertEquals(customerDTO.firstName, createdCustomer.firstName);
        assertEquals(customerDTO.lastName, createdCustomer.lastName);
        assertEquals(customerDTO.cpr, createdCustomer.cpr);
        assertEquals(createdCustomer.instanceId, customerId);
    }

    @Given("a customer creation request is received")
    public void aCustomerCreationRequestIsReceived() {
        customerDTO = new CustomerDTO("first", "last", "cpr");
    }

    @And("a customer creation request with the same cpr is received")
    public void aCustomerCreationRequestWithTheSameCprIsReceived() {
        invalidatedCreatedCustomer = new CustomerDTO("first-inva", "last-inva", "cpr");
    }

    @When("Creating a customer with the same cpr")
    public void creatingACustomerWithTheSameCpr() {
        new Thread(() -> {
            ValidatedResponseFuture.complete(
                    administrationService2.createCustomer(customerDTO.firstName,
                            customerDTO.lastName,
                            customerDTO.cpr));
        }).start();
        new Thread(() -> {
            InvalidResponseFuture.complete(administrationService3.createCustomer(invalidatedCreatedCustomer.firstName,
                    invalidatedCreatedCustomer.lastName,
                    invalidatedCreatedCustomer.cpr));
        }).start();
    }

    @Then("Return unsuccessful response")
    public void returnUnsuccessfulResponse() {
        DTUPayEmptyResponse response1 = ValidatedResponseFuture.join();
        DTUPayEmptyResponse response2 = InvalidResponseFuture.join();
        assertTrue(response1.success());
        assertFalse(response2.success());
    }
}
