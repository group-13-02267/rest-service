package servicetests;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources", snippets=SnippetType.CAMELCASE, plugin = {"pretty"})

public class RunCucumberTest {
}
