package servicetests;

import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayResponse;
import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.helpers.DTUFutureWrapper;
import dtu_pay.services.reporting.DTO.ReportDTO;
import dtu_pay.services.reporting.DTO.ReportGenerationRequestDTO;
import dtu_pay.services.reporting.customer.CustomerReportGenerationService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertNotNull;

public class ReportSteps {

    CustomerReportGenerationService customerReportGenerationService;
    CompletableFuture<DTUPayResponse<ReportDTO>> response = new CompletableFuture<>();
    MessageSender reportMessageSender;
    String requestId = UUID.randomUUID().toString();
    CompletableFuture<ReportGenerationRequestDTO> reportGenerationFuture = new CompletableFuture<>();

    public ReportSteps() {
        reportMessageSender = new RabbitMqMessageService() {
            @Override
            public void sendMessage(Object content, String route, String exchange) {
                reportGenerationFuture.complete((ReportGenerationRequestDTO) content);
                Assert.assertEquals("reporting", exchange);
                Assert.assertEquals("reporting.customer.request." + requestId , route);
            }
        };

        customerReportGenerationService = new CustomerReportGenerationService(
            requestId,
            "report.generated." + requestId,
            "reporting.customer.request." + requestId,
            reportMessageSender,
            new DTUFutureWrapper()
        );
    }

    @And("the customer request a report")
    public void theCustomerRequestAReport() {
        new Thread(() -> {
            try {
                response.complete(
                        customerReportGenerationService.generateCustomerReport(
                                "some_customer",
                                "01-01-1990",
                                "20-20-2020"
                        )
                );
            } catch (Exception e) {
                throw new Error(e);
            }
        }).start();
    }

    @Then("the report request event is broadcast")
    public void theReportRequestEventIsBroadcast() {
         ReportGenerationRequestDTO reportGenerationRequestDTO = reportGenerationFuture.join();
    }

    @Then("a report with the payment is received")
    public void aReportWithThePaymentIsReceived() {
        customerReportGenerationService.receive(new ReportDTO());
        DTUPayResponse<ReportDTO> payResponse = response.join();

        ReportDTO reportDTO = payResponse.getResponse();

        assertNotNull(reportDTO);
    }
}
