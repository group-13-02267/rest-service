package servicetests;

import dtu_pay.common.MessageSender;
import dtu_pay.rest.core.DTUPayEmptyResponse;
import dtu_pay.services.RabbitMqMessageService;
import dtu_pay.services.administration.AdministrationServiceImpl;
import dtu_pay.services.administration.DTO.MerchantDTO;
import dtu_pay.services.payment.DTO.ResponseDTO;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

public class MerchantSteps {

    AdministrationServiceImpl administrationService;
    MessageSender merchantSender;
    CompletableFuture<DTUPayEmptyResponse> response = new CompletableFuture<>();
    CompletableFuture<MerchantDTO> merchantFeature = new CompletableFuture<>();
    String instanceId = UUID.randomUUID().toString();
    List<String> existingMerchantCprs = new ArrayList<>();

    MerchantDTO lastMerchantRequest;

    public MerchantSteps() {
        merchantSender = new RabbitMqMessageService() {
            @Override
            public void sendMessage(Object content, String route, String exchange) {
                merchantFeature.complete((MerchantDTO) content);
                assertEquals("administration.merchant.create." + instanceId, route);
                assertEquals("administration", exchange);
            }
        };
        administrationService = new AdministrationServiceImpl("merchant", instanceId, merchantSender);
    }

    private void createMerchant(String firstName, String lastName, String cpr) {
        MerchantDTO request = new MerchantDTO();
        request.firstName = firstName;
        request.lastName = lastName;
        request.cpr = cpr;
        lastMerchantRequest = request;

        new Thread(() -> {
            try {
                response.complete(administrationService.createMerchant(request.firstName,
                        request.lastName, request.cpr));
            } catch (Exception e) {
                throw new Error(e);
            }
        }).start();
    }

    @Given("an existing merchant")
    public void givenAnExistingMerchant() {
        existingMerchantCprs.add(String.valueOf(new Random().nextInt(9999999)));
    }

    @When("a request for a new valid merchant is received")
    public void whenARequestForANewValidMerchantIsReceived() {
        createMerchant("first", "last", String.valueOf(new Random().nextInt(9999999)));
    }

    @When("a request for a merchant with a duplicate cpr is received")
    public void whenARequestForAMerchantWithADuplicateCpr() {
        createMerchant("first2", "last2", existingMerchantCprs.get(0));
    }

    @Then("an event response is received")
    public void anEventResponseIsReceived() {
        if (existingMerchantCprs.contains(lastMerchantRequest.cpr)) {
            administrationService.receive(new ResponseDTO(false));
        } else {
            administrationService.receive(new ResponseDTO(true));
        }
    }

    @Then("the merchant request event is broadcast")
    public void thenTheMerchantRequestEventIsBroadcast() {
        MerchantDTO merchantDTO = merchantFeature.join();
        assertEquals(lastMerchantRequest.cpr, merchantDTO.cpr);
        assertEquals(lastMerchantRequest.firstName, merchantDTO.firstName);
        assertEquals(lastMerchantRequest.lastName, merchantDTO.lastName);
    }

    @Then("the merchant request succeeds")
    public void thenTheRequestSucceeds() {
        assertTrue(response.join().success());
    }

    @Then("an unsuccessful response is returned")
    public void thenAnUnsuccessfulResponseIsReturned() {
        assertFalse(response.join().success());
    }

}
